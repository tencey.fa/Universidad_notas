
package universidad_notas;
import java.util.Scanner;

public class Universidad_notas {
    
    
    public static void Inicializar(double Calificaciones[], double Fisica[], double Quimica[], String Alumnos[], String Id[], String Edad[], double P1[], double P2[], double P3[], double P3_1[], double P4[], Scanner Teclado)
    {
        for (int i = 0; i < Calificaciones.length; i++)
        {
            System.out.print("Nombre del alumno ");
            Alumnos[i] = Teclado.next();            
            System.out.print("Id del alumno ");
            Id[i] = Teclado.next();
            System.out.print("Edad del alumno ");
            Edad[i] = Teclado.next();
            System.out.print("Ingrese las notas para la materia matematicas ");
            P1[i] = Teclado.nextDouble();
            P2[i] = Teclado.nextDouble();
            P3[i] = Teclado.nextDouble();
            P3_1[i] = Teclado.nextDouble();
            P4[i] = Teclado.nextDouble(); 
            P3[i] = P3[i] * 5;
            P3_1[i] = P3_1[i] * 7;
            Calificaciones[i] = P1[i] + P2[i] + P3[i] +P3_1[i] + P4[i];
            
            System.out.print("Ingrese las notas para la materia Fisica ");
            P1[i] = Teclado.nextDouble();
            P2[i] = Teclado.nextDouble();
            P3[i] = Teclado.nextDouble();
            P3_1[i] = Teclado.nextDouble();
            P4[i] = Teclado.nextDouble(); 
            P3[i] = P3[i] * 5;
            P3_1[i] = P3_1[i] * 7;
            Fisica[i] = P1[i] + P2[i] + P3[i] + P3_1[i] + P4[i];
            
            System.out.print("Ingrese las notas para la materia Quimica ");
            P1[i] = Teclado.nextDouble();
            P2[i] = Teclado.nextDouble();
            P3[i] = Teclado.nextDouble();
            P3_1[i] = Teclado.nextDouble();
            P4[i] = Teclado.nextDouble(); 
            P3[i] = P3[i] * 5;
            P3_1[i] = P3_1[i] * 7;
            Quimica[i] = P1[i] + P2[i] + P3[i] + P3_1[i] + P4[i];
            
        
        }
    }
    
    
    public static double Promedio(double Calificaciones[])
    {
        double prom, suma = 0;
        for (int i = 0; i < Calificaciones.length; i++)
            suma += Calificaciones[i];
        prom = suma / Calificaciones.length / 15.0 ;       
        return prom;
    }
    public static double Promedio1(double Fisica[])
    {
        double prom, suma = 0;
        for (int i = 0; i < Fisica.length; i++)
               suma += Fisica[i];
        prom = suma / Fisica.length / 15.0;        
        return prom;
    }
    
    public static double Promedio2(double Quimica[])
    {
        double prom, suma = 0;
        for (int i = 0; i < Quimica.length; i++)
            suma += Quimica[i];
        prom = suma / Quimica.length / 15.0;        
        return prom;
    }
    
    public static void Imprimir(double Calificaciones[], String Alumnos[], String Id[], String Edad[], double P1[], double P2[], double P3[], double P3_1[], double P4[], double Promedio)
    {
        System.out.printf("%-10s %-10s %-10s %-10s%n%n", "Alumnos", "Id", "Edad", "Calificacion");
        for (int i = 0; i < Calificaciones.length; i++)
        {            
            System.out.printf("%-10s %-10s %-10s %-10f%n", Alumnos[i], Id[i], Edad[i], Promedio);
        }
        System.out.println("\nEl promedio grupal es "+Promedio);
    }
    
    public static void Mayor (double Calificaciones[], double P1[],double P2[], double P3[], double P3_1[], double P4[], double Fisica[], double Quimica[], String Alumnos[])
    {
        
        double Mayor;    
        
        
        for (int i = 0; i < Calificaciones.length; i++)
               Calificaciones[i] = Calificaciones[i] / 15.0;
        for (int i = 0; i < Calificaciones.length; i++)
            if ( Calificaciones[i] > 3.5)
            {                  
                 //operaciones  
            P1[i] = 100 * P1[i] / 25;
            P2[i] = 100 * P2[i] / 25;            
            P3[i] = 100 * P3[i] / 20 ;
            P3_1[i] = 100 * P3_1[i] / 20;
            P4[i] = 100 * P4[i] / 30;     
            P3[i] = 5 * P3[i];
            P3_1[i] = 7 * P3_1[i];            
            Mayor = Calificaciones[i] ;
                System.out.println("\nLa calificacion mas alta para matematicas fue "+Mayor+" y la saco "+Alumnos[i]); 
                System.out.println("Recibe mencion de honor "+Alumnos[i]);            
               
            }      
            else{
                Mayor = Calificaciones[i];
                System.out.println("\nLa calificacion menor para matematicas fue "+Mayor+" y la saco "+Alumnos[i]);
                System.out.println("Recibe condicionamiento "+Alumnos[i]);
            }        
         
        for (int i = 0; i < Calificaciones.length; i++)
               Fisica[i] = Fisica[i] / 15.0;
        for (int i = 0; i < Fisica.length; i++)
            if (Fisica[i] > 3.5)
            {          
                Mayor = Fisica[i];                
                System.out.println("\nLa calificacion mas alta para fisica fue "+Mayor+" y la saco "+Alumnos[i]); 
                System.out.println("Recibe mencion de honor "+Alumnos[i]);               
            }     
        else{
                Mayor = Fisica[i];
                System.out.println("\nLa calificacion menor para fisica fue "+Mayor+" y la saco "+Alumnos[i]);
                System.out.println("Recibe condicionamiento "+Alumnos[i]);
            } 
        
        for (int i = 0; i < Calificaciones.length; i++)
               Quimica[i] = Quimica[i] / 15.0;
        for (int i = 0; i < Quimica.length; i++)
            if (Quimica[i] > 3.5)
            {        
                Mayor = Quimica[i];                
                System.out.println("\nLa calificacion mas alta para quimica fue "+Mayor+" y la saco "+Alumnos[i]); 
                System.out.println("Recibe mencion de honor "+Alumnos[i]);              
            }        
        else{
                Mayor = Quimica[i];
                System.out.println("\nLa calificacion menor para quimica fue "+Mayor+" y la saco "+Alumnos[i]);
                System.out.println("Recibe condicionamiento "+Alumnos[i]);
            } 
           
    }
    
    public static void main(String[] args) {
        Scanner Teclado = new Scanner(System.in);
        
        double Calificaciones[];
        double Fisica [];
        double Quimica [];
        String Alumnos[];        
        String Id[];
        String Edad[];
        int n;
        double Promedio;
        double P1[];
        double P2[];
        double P3[];
        double P3_1[];
        double P4[];
        
        
        
        System.out.print("Ingrese la cantidad de alumnos ");
        n = Teclado.nextInt();
        
        Calificaciones = new double[n];
        Fisica = new double [n];
        Quimica = new double [n];        
        Alumnos = new String[n];
        Id = new String[n];
        Edad = new String[n];
        P1 = new double[n];
        P2 = new double[n];
        P3 = new double[n];
        P3_1 = new double[n];
        P4 = new double[n];
        
        
        Inicializar(Calificaciones, Fisica, Quimica, Alumnos, Id, Edad, P1, P2, P3, P3_1, P4, Teclado);        
        Promedio = Promedio(Calificaciones);
        Promedio = Promedio1(Fisica);
        Promedio = Promedio2(Quimica);
        Imprimir(Calificaciones, Alumnos, Id, Edad, P1, P2, P3, P3_1, P4, Promedio);
        Mayor(Calificaciones, P1, P2, P3, P3_1, P4, Fisica, Quimica, Alumnos);
        
    }
}
